Exercício M02-E01
=================

Objetivos
---------

- Aprender a criar um repositório Git no [GitHub](https://www.github.com); e
- Aprender a clonar o repositório na máquina local.

Instruções
----------

1. Crie o diretório para a resolução dos exercícios do módulo 2:

    ```bash
      mkdir -p ~/workspace/ep-exercicios/m2
    ```

2. Nesta aula, não será necessário criar outros diretórios específicos para cada um dos exercícios dentro do diretório criado no passo anterior. Isso é devido ao fato de todos os exercícios se baserem em repositório Git, ou seja, em um único diretório.

3. Crie uma conta no [GitHub](https://www.github.com), caso você ainda não tenha uma.

4. Através da interface web do GitHub, crie um novo repositório na sua conta com o nome `ep-m2-repo`.

5. Agora, você irá clonar o repositório remoto criado no Github para o seu ambiente local no Cloud9. O diretório do repositório Git deve ser clonado dentro do diretório destinado aos exercícios do módulo 2. Então, após clonar o respositório, deve existir o seguinte diretório no seu sistema de arquivos:

    ```bash
      ~/workspace/ep-exercicios/m2/ep-m2-repo
    ```

6. Para clonar o repositório, deverá ser usado o comando `git clone`. Você pode usar esse comando fornecendo dois parâmetros:
  - O endereço (URL) do repositório remoto (que pode ser encontrado no Github); e
  - O nome diretório a ser criado localmente para receber o repositório.

    Então, use o comando da seguinte forma:

    ```bash
      git clone <repositorio> <diretorio>
    ```

    **Dica:** para fazer o clone do GitHub, utilize a opção __HTTPS__ ao invés de __SSH__.

7. Utilize a ferramenta de correção automática do `ep-cli` para verificar se está tudo certo.

    ```bash
      ep-cli corrigir --dir ~/workspace/ep-exercicios/m2 --dicas 2.1
    ```

8. Quando você tiver finalizado o exercício, utilize a ferramenta `ep-cli`
   para enviar o resultado.

    ```bash
      ep-cli enviar 2.1
    ```
